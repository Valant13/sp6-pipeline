<?php

namespace App\Service;

use App\Config;
use App\Entity\Source\Apartment;
use App\Entity\Source\Lot;
use App\Entity\Source\Material;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SourceService
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return int
     */
    public function getCurrentYear(): int
    {
        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/period/year'
        );

        return (int)$response->getContent();
    }

    /**
     * @return Apartment
     */
    public function downloadApartment(): Apartment
    {
        $apartment = new Apartment();

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/apartment/apartments'
        );
        $apartment->setApartments(json_decode($response->getContent(), true));

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/apartment/apartment-invoices'
        );
        $apartment->setApartmentInvoices(json_decode($response->getContent(), true));

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/apartment/buildings'
        );
        $apartment->setBuildings(json_decode($response->getContent(), true));

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/apartment/building-projects'
        );
        $apartment->setBuildingProjects(json_decode($response->getContent(), true));

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/apartment/demands'
        );
        $apartment->setDemands(json_decode($response->getContent(), true));

        return $apartment;
    }

    /**
     * @return Lot
     */
    public function downloadLot(): Lot
    {
        $lot = new Lot();

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/lot/lot-invoices'
        );
        $lot->setLotInvoices(json_decode($response->getContent(), true));

        return $lot;
    }

    /**
     * @return Material
     */
    public function downloadMaterial(): Material
    {
        $material = new Material();

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/material/materials'
        );
        $material->setMaterials(json_decode($response->getContent(), true));

        $response = $this->client->request(
            'GET',
            Config::CONSTRUCTION_COMPANY_URL . '/material/material-invoices'
        );
        $material->setMaterialInvoices(json_decode($response->getContent(), true));

        return $material;
    }
}
