<?php

namespace App\Service;

use App\Config;
use App\Entity\Destination\Construction;
use App\Entity\Destination\Sale;
use App\Entity\Source\Apartment;
use App\Entity\Source\Lot;
use App\Entity\Source\Material;

class TransformationService
{
    /**
     * @param Apartment $apartmentBag
     * @param Lot $lotBag
     * @param Material $materialBag
     * @return Construction
     */
    public function createConstruction(Apartment $apartmentBag, Lot $lotBag, Material $materialBag): Construction
    {
        $projects = [];
        foreach ($apartmentBag->getBuildingProjects() as $buildingProject) {
            $index = $buildingProject['id'];

            $projects[$index] = [
                'id' => $buildingProject['id'],
                'building_type' => $buildingProject['building_type'],
                'total_area' => $buildingProject['one_bedroom_qty'] * Config::ONE_BEDROOM_APARTMENT_AREA +
                    $buildingProject['two_bedroom_qty'] * Config::TWO_BEDROOM_APARTMENT_AREA +
                    $buildingProject['three_bedroom_qty'] * Config::THREE_BEDROOM_APARTMENT_AREA,
                'steel_need' => $buildingProject['steel_qty'],
                'concrete_need' => $buildingProject['concrete_qty'],
                'bricks_need' => $buildingProject['bricks_qty'],
                'lot_area_need' => $buildingProject['min_lot_area'],
                'development_date' => $buildingProject['year']
            ];
        }

        $lotInvoices = [];
        foreach ($lotBag->getLotInvoices() as $lotInvoice) {
            $index = $lotInvoice['building_id'];

            $lotInvoices[$index] = [
                'id' => $lotInvoice['id'],
                'lot_area' => 0,
                'date' => $lotInvoice['year'],
                'total' => $lotInvoice['total']
            ];
        }

        $indexedBuildingProjects = $this->indexEntities($apartmentBag->getBuildingProjects(), 'id');
        $buildingMaterials = [];
        foreach ($apartmentBag->getBuildings() as $building) {
            $index = $building['id'];
            $buildingProjectId = $building['project_id'];

            $buildingMaterials[$index] = [
                'steel_qty' => $indexedBuildingProjects[$buildingProjectId]['steel_qty'],
                'concrete_qty' => $indexedBuildingProjects[$buildingProjectId]['concrete_qty'],
                'bricks_qty' => $indexedBuildingProjects[$buildingProjectId]['bricks_qty'],
            ];
        }

        $mappedMaterials = $this->mapEntities($materialBag->getMaterials(), 'material_type', 'year');
        $materialInvoices = [];
        foreach ($materialBag->getMaterialInvoices() as $materialInvoice) {
            $index = $materialInvoice['building_id'];
            $year = $materialInvoice['year'];

            $materialInvoices[$index] = [
                'id' => $materialInvoice['id'],
                'steel_price' => $mappedMaterials['steel'][$year]['price'],
                'steel_qty' => $buildingMaterials[$index]['steel_qty'],
                'concrete_price' => $mappedMaterials['concrete'][$year]['price'],
                'concrete_qty' => $buildingMaterials[$index]['concrete_qty'],
                'bricks_price' => $mappedMaterials['bricks'][$year]['price'],
                'bricks_qty' => $buildingMaterials[$index]['bricks_qty'],
                'date' => $year,
                'total' => $materialInvoice['total']
            ];
        }

        $constructions = [];
        foreach ($apartmentBag->getBuildings() as $building) {
            $index = $building['id'];

            $constructions[$index] = [
                'id' => $building['id'],
                'project_id' => $building['project_id'],
                'material_invoice_id' => $materialInvoices[$index]['id'],
                'lot_invoice_id' => $lotInvoices[$index]['id'],
                'date' => $building['year']
            ];
        }

        $constructionBag = new Construction();
        $constructionBag->setMaterialInvoices($materialInvoices);
        $constructionBag->setLotInvoices($lotInvoices);
        $constructionBag->setProjects($projects);
        $constructionBag->setConstructions($constructions);

        return $constructionBag;
    }

    /**
     * @param Apartment $apartmentBag
     * @param Lot $lotBag
     * @param Material $materialBag
     * @return Sale
     */
    public function createSale(Apartment $apartmentBag, Lot $lotBag, Material $materialBag): Sale
    {
        $apartments = [];
        foreach ($apartmentBag->getApartments() as $apartment) {
            $index = $apartment['id'];

            $apartments[$index] = [
                'id' => $apartment['id'],
                'apartment_type' => $apartment['apartment_type'],
                'area' => $apartment['area'],
                'building_id' => $apartment['building_id']
            ];
        }

        $indexedBuildingProjects = $this->indexEntities($apartmentBag->getBuildingProjects(), 'id');
        $indexedLotInvoices = $this->indexEntities($lotBag->getLotInvoices(), 'building_id');
        $indexedMaterialInvoices = $this->indexEntities($materialBag->getMaterialInvoices(), 'building_id');
        $buildings = [];
        foreach ($apartmentBag->getBuildings() as $building) {
            $index = $building['id'];
            $buildingProject = $indexedBuildingProjects[$building['project_id']];

            $buildings[$index] = [
                'id' => $building['id'],
                'building_type' => $buildingProject['building_type'],
                'total_area' => $buildingProject['one_bedroom_qty'] * Config::ONE_BEDROOM_APARTMENT_AREA +
                    $buildingProject['two_bedroom_qty'] * Config::TWO_BEDROOM_APARTMENT_AREA +
                    $buildingProject['three_bedroom_qty'] * Config::THREE_BEDROOM_APARTMENT_AREA,
                'construction_date' => $building['year'],
                'construction_cost' => $indexedLotInvoices[$index]['total'] + $indexedMaterialInvoices[$index]['total']
            ];
        }

        $periods = [];
        foreach ($apartmentBag->getDemands() as $demand) {
            $index = $demand['id'];

            $periods[$index] = [
                'id' => $demand['id'],
                'year' => $demand['year'],
                'apartment_type' => $demand['apartment_type'],
                'building_type' => $demand['building_type'],
                'demand' => $demand['apartment_qty']
            ];
        }

        $mappedDemands = $this->mapEntities(
            $apartmentBag->getDemands(),
            'apartment_type',
            'building_type',
            'year'
        );
        $customers = [];
        $sales = [];
        foreach ($apartmentBag->getApartmentInvoices() as $apartmentInvoice) {
            $index = $apartmentInvoice['id'];

            $customers[$index] = [
                'id' => $apartmentInvoice['id'],
                'first_name' => $apartmentInvoice['customer_first_name'],
                'last_name' => $apartmentInvoice['customer_last_name']
            ];

            $apartmentId = $apartmentInvoice['apartment_id'];
            $buildingId = $apartments[$apartmentId]['building_id'];
            $apartmentType = $apartments[$apartmentId]['apartment_type'];
            $buildingType = $buildings[$buildingId]['building_type'];
            $year = $apartmentInvoice['year'];
            $sales[$index] = [
                'id' => $apartmentInvoice['id'],
                'period_id' => $mappedDemands[$apartmentType][$buildingType][$year]['id'],
                'apartment_id' => $apartmentId,
                'building_id' => $buildingId,
                'customer_id' => $apartmentInvoice['id'],
                'date' => $year,
                'total' => $apartmentInvoice['total']
            ];
        }

        $sale = new Sale();
        $sale->setApartments($apartments);
        $sale->setBuildings($buildings);
        $sale->setCustomers($customers);
        $sale->setPeriods($periods);
        $sale->setSales($sales);

        return $sale;
    }

    /**
     * @param array $entities
     * @param string $indexField
     * @return array
     */
    private function indexEntities(array $entities, string $indexField): array
    {
        $indexedEntities = [];
        foreach ($entities as $entity) {
            $index = $entity[$indexField];
            $indexedEntities[$index] = $entity;
        }

        return $indexedEntities;
    }

    /**
     * @param array $entities
     * @param string $groupField
     * @param string $sumField
     * @return int[]
     */
    private function groupAndSumEntities(array $entities, string $groupField, string $sumField): array
    {
        $sum = [];
        foreach ($entities as $entity) {
            $index = $entity[$groupField];

            if (!array_key_exists($index, $sum)) {
                $sum[$index] = 0;
            }

            $sum[$index] += $entity[$sumField];
        }

        return $sum;
    }

    /**
     * @param array $entities
     * @param string $firstIndexField
     * @param string $secondIndexField
     * @param string|null $thirdIndexField
     * @return array
     */
    private function mapEntities(
        array $entities,
        string $firstIndexField,
        string $secondIndexField,
        string $thirdIndexField = null
    ): array {
        $map = [];
        foreach ($entities as $entity) {
            $firstIndex = $entity[$firstIndexField];
            $secondIndex = $entity[$secondIndexField];

            if (!array_key_exists($firstIndex, $map)) {
                $map[$firstIndex] = [];
            }

            if ($thirdIndexField === null) {
                $map[$firstIndex][$secondIndex] = $entity;
            } else {
                $thirdIndex = $entity[$thirdIndexField];

                if (!array_key_exists($secondIndex, $map[$firstIndex])) {
                    $map[$firstIndex][$secondIndex] = [];
                }

                $map[$firstIndex][$secondIndex][$thirdIndex] = $entity;
            }
        }

        return $map;
    }
}
