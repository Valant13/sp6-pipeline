<?php

namespace App\Service;

use App\Config;
use App\Entity\Destination\Construction;
use App\Entity\Destination\Sale;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DestinationService
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $currentYear
     */
    public function setCurrentYear(int $currentYear): void
    {
        $this->client->request(
            'POST',
            Config::DASHBOARD_URL . "/period/year/$currentYear"
        );
    }

    /**
     * @param Construction $constructionBag
     */
    public function uploadConstruction(Construction $constructionBag): void
    {
        $projectIdMap = $this->uploadEntities(
            $constructionBag->getProjects(),
            Config::DASHBOARD_URL . '/construction/projects'
        );

        $lotInvoiceIdMap = $this->uploadEntities(
            $constructionBag->getLotInvoices(),
            Config::DASHBOARD_URL . '/construction/lot-invoices'
        );

        $materialInvoiceIdMap = $this->uploadEntities(
            $constructionBag->getMaterialInvoices(),
            Config::DASHBOARD_URL . '/construction/material-invoices'
        );

        $processedConstructions = [];
        $constructions = $constructionBag->getConstructions();
        foreach ($constructions as $construction) {
            $processedConstruction = [
                'id' => $construction['id'],
                'project_id' => $projectIdMap[$construction['project_id']],
                'lot_invoice_id' => $lotInvoiceIdMap[$construction['lot_invoice_id']],
                'material_invoice_id' => $materialInvoiceIdMap[$construction['material_invoice_id']],
                'date' => $construction['date']
            ];

            $processedConstructions[] = $processedConstruction;
        }

        $this->uploadEntities(
            $processedConstructions,
            Config::DASHBOARD_URL . '/construction/constructions'
        );
    }

    /**
     * @param Sale $saleBag
     */
    public function uploadSale(Sale $saleBag): void
    {
        $apartmentIdMap = $this->uploadEntities(
            $saleBag->getApartments(),
            Config::DASHBOARD_URL . '/sale/apartments'
        );

        $buildingIdMap = $this->uploadEntities(
            $saleBag->getBuildings(),
            Config::DASHBOARD_URL . '/sale/buildings'
        );

        $customerIdMap = $this->uploadEntities(
            $saleBag->getCustomers(),
            Config::DASHBOARD_URL . '/sale/customers'
        );

        $periodIdMap = $this->uploadEntities(
            $saleBag->getPeriods(),
            Config::DASHBOARD_URL . '/sale/periods'
        );

        $processedSales = [];
        $sales = $saleBag->getSales();
        foreach ($sales as $sale) {
            $processedSale = [
                'id' => $sale['id'],
                'period_id' => $periodIdMap[$sale['period_id']],
                'apartment_id' => $apartmentIdMap[$sale['apartment_id']],
                'building_id' => $buildingIdMap[$sale['building_id']],
                'customer_id' => $customerIdMap[$sale['customer_id']],
                'date' => $sale['date'],
                'total' => $sale['total']
            ];

            $processedSales[] = $processedSale;
        }

        $this->uploadEntities(
            $processedSales,
            Config::DASHBOARD_URL . '/sale/sales'
        );
    }

    /**
     * @param array $entities
     * @param string $endpointUrl
     * @return int[]
     */
    private function uploadEntities(array $entities, string $endpointUrl): array
    {
        $response = $this->client->request(
            'POST',
            $endpointUrl,
            ['json' => $entities]
        );

        return $this->mapEntityIds($entities, json_decode($response->getContent(), true));
    }

    /**
     * @param array $entities
     * @param int[] $entityIds
     * @return int[]
     */
    private function mapEntityIds(array $entities, array $entityIds): array
    {
        $entitiesCount = count($entities);

        $entityKeys = array_keys($entities);
        $entityIdKeys = array_keys($entityIds);

        $entityIdMap = [];
        for ($i = 0; $i < $entitiesCount; $i++) {
            $entity = $entities[$entityKeys[$i]];
            $entityId = $entityIds[$entityIdKeys[$i]];

            $entityIdMap[$entity['id']] = $entityId;
        }

        return $entityIdMap;
    }
}
