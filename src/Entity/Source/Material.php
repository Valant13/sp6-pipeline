<?php

namespace App\Entity\Source;

class Material
{
    /**
     * @var array
     */
    private $materials;

    /**
     * @var array
     */
    private $materialInvoices;

    /**
     * @return array
     */
    public function getMaterials(): array
    {
        return $this->materials;
    }

    /**
     * @param array $materials
     */
    public function setMaterials(array $materials): void
    {
        $this->materials = $materials;
    }

    /**
     * @return array
     */
    public function getMaterialInvoices(): array
    {
        return $this->materialInvoices;
    }

    /**
     * @param array $materialInvoices
     */
    public function setMaterialInvoices(array $materialInvoices): void
    {
        $this->materialInvoices = $materialInvoices;
    }
}
