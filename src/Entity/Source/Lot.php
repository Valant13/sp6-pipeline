<?php

namespace App\Entity\Source;

class Lot
{
    /**
     * @var array
     */
    private $lotInvoices;

    /**
     * @return array
     */
    public function getLotInvoices(): array
    {
        return $this->lotInvoices;
    }

    /**
     * @param array $lotInvoices
     */
    public function setLotInvoices(array $lotInvoices): void
    {
        $this->lotInvoices = $lotInvoices;
    }
}
