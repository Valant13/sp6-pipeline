<?php

namespace App\Entity\Source;

class Apartment
{
    /**
     * @var array
     */
    private $apartments;

    /**
     * @var array
     */
    private $apartmentInvoices;

    /**
     * @var array
     */
    private $buildings;

    /**
     * @var array
     */
    private $buildingProjects;

    /**
     * @var array
     */
    private $demands;

    /**
     * @return array
     */
    public function getApartments(): array
    {
        return $this->apartments;
    }

    /**
     * @param array $apartments
     */
    public function setApartments(array $apartments): void
    {
        $this->apartments = $apartments;
    }

    /**
     * @return array
     */
    public function getApartmentInvoices(): array
    {
        return $this->apartmentInvoices;
    }

    /**
     * @param array $apartmentInvoices
     */
    public function setApartmentInvoices(array $apartmentInvoices): void
    {
        $this->apartmentInvoices = $apartmentInvoices;
    }

    /**
     * @return array
     */
    public function getBuildings(): array
    {
        return $this->buildings;
    }

    /**
     * @param array $buildings
     */
    public function setBuildings(array $buildings): void
    {
        $this->buildings = $buildings;
    }

    /**
     * @return array
     */
    public function getBuildingProjects(): array
    {
        return $this->buildingProjects;
    }

    /**
     * @param array $buildingProjects
     */
    public function setBuildingProjects(array $buildingProjects): void
    {
        $this->buildingProjects = $buildingProjects;
    }

    /**
     * @return array
     */
    public function getDemands(): array
    {
        return $this->demands;
    }

    /**
     * @param array $demands
     */
    public function setDemands(array $demands): void
    {
        $this->demands = $demands;
    }
}
