<?php

namespace App\Entity\Destination;

class Sale
{
    /**
     * @var array
     */
    private $apartments;

    /**
     * @var array
     */
    private $buildings;

    /**
     * @var array
     */
    private $customers;

    /**
     * @var array
     */
    private $periods;

    /**
     * @var array
     */
    private $sales;

    /**
     * @return array
     */
    public function getApartments(): array
    {
        return $this->apartments;
    }

    /**
     * @param array $apartments
     */
    public function setApartments(array $apartments): void
    {
        $this->apartments = $apartments;
    }

    /**
     * @return array
     */
    public function getBuildings(): array
    {
        return $this->buildings;
    }

    /**
     * @param array $buildings
     */
    public function setBuildings(array $buildings): void
    {
        $this->buildings = $buildings;
    }

    /**
     * @return array
     */
    public function getCustomers(): array
    {
        return $this->customers;
    }

    /**
     * @param array $customers
     */
    public function setCustomers(array $customers): void
    {
        $this->customers = $customers;
    }

    /**
     * @return array
     */
    public function getPeriods(): array
    {
        return $this->periods;
    }

    /**
     * @param array $periods
     */
    public function setPeriods(array $periods): void
    {
        $this->periods = $periods;
    }

    /**
     * @return array
     */
    public function getSales(): array
    {
        return $this->sales;
    }

    /**
     * @param array $sales
     */
    public function setSales(array $sales): void
    {
        $this->sales = $sales;
    }
}
