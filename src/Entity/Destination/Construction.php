<?php

namespace App\Entity\Destination;

class Construction
{
    /**
     * @var array
     */
    private $constructions;

    /**
     * @var array
     */
    private $projects;

    /**
     * @var array
     */
    private $lotInvoices;

    /**
     * @var array
     */
    private $materialInvoices;

    /**
     * @return array
     */
    public function getConstructions(): array
    {
        return $this->constructions;
    }

    /**
     * @param array $constructions
     */
    public function setConstructions(array $constructions): void
    {
        $this->constructions = $constructions;
    }

    /**
     * @return array
     */
    public function getProjects(): array
    {
        return $this->projects;
    }

    /**
     * @param array $projects
     */
    public function setProjects(array $projects): void
    {
        $this->projects = $projects;
    }

    /**
     * @return array
     */
    public function getLotInvoices(): array
    {
        return $this->lotInvoices;
    }

    /**
     * @param array $lotInvoices
     */
    public function setLotInvoices(array $lotInvoices): void
    {
        $this->lotInvoices = $lotInvoices;
    }

    /**
     * @return array
     */
    public function getMaterialInvoices(): array
    {
        return $this->materialInvoices;
    }

    /**
     * @param array $materialInvoices
     */
    public function setMaterialInvoices(array $materialInvoices): void
    {
        $this->materialInvoices = $materialInvoices;
    }
}
