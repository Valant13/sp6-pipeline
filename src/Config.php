<?php

namespace App;

class Config
{
    const CONSTRUCTION_COMPANY_URL = 'http://construction-company.local';
    const DASHBOARD_URL = 'http://dashboard.local';

    const ONE_BEDROOM_APARTMENT_AREA = 25;
    const TWO_BEDROOM_APARTMENT_AREA = 50;
    const THREE_BEDROOM_APARTMENT_AREA = 75;
}
