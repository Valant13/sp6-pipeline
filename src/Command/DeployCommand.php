<?php

namespace App\Command;

use App\Service\DestinationService;
use App\Service\SourceService;
use App\Service\TransformationService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DeployCommand extends Command
{
    protected static $defaultName = 'app:deploy';

    /**
     * @var SourceService
     */
    private $sourceService;

    /**
     * @var DestinationService
     */
    private $destinationService;

    /**
     * @var TransformationService
     */
    private $transformationService;

    /**
     * @param SourceService $sourceService
     * @param DestinationService $destinationService
     * @param TransformationService $transformationService
     * @param string|null $name
     */
    public function __construct(
        SourceService $sourceService,
        DestinationService $destinationService,
        TransformationService $transformationService,
        string $name = null
    ) {
        parent::__construct($name);
        $this->sourceService = $sourceService;
        $this->destinationService = $destinationService;
        $this->transformationService = $transformationService;
    }

    /**
     *
     */
    protected function configure(): void
    {
        $this->setDescription('Deploy data');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Deployment started');
        $io->newLine();

        $io->write('Getting current year...');
        $currentYear = $this->sourceService->getCurrentYear();
        $io->writeln(' <fg=green>Done</>');

        $io->write('Setting current year...');
        $this->destinationService->setCurrentYear($currentYear);
        $io->writeln(' <fg=green>Done</>');

        $io->write('Downloading lots...');
        $lotBag = $this->sourceService->downloadLot();
        $io->writeln(' <fg=green>Done</>');

        $io->write('Downloading materials...');
        $materialBag = $this->sourceService->downloadMaterial();
        $io->writeln(' <fg=green>Done</>');

        $io->write('Downloading apartments...');
        $apartmentBag = $this->sourceService->downloadApartment();
        $io->writeln(' <fg=green>Done</>');

        $io->write('Creating constructions...');
        $constructionBag = $this->transformationService->createConstruction($apartmentBag, $lotBag, $materialBag);
        $io->writeln(' <fg=green>Done</>');

        $io->write('Creating sales...');
        $saleBag = $this->transformationService->createSale($apartmentBag, $lotBag, $materialBag);
        $io->writeln(' <fg=green>Done</>');

        $io->write('Uploading constructions...');
        $this->destinationService->uploadConstruction($constructionBag);
        $io->writeln(' <fg=green>Done</>');

        $io->write('Uploading sales...');
        $this->destinationService->uploadSale($saleBag);
        $io->writeln(' <fg=green>Done</>');

        $io->newLine();
        $io->writeln('Deployment finished');

        return Command::SUCCESS;
    }
}
